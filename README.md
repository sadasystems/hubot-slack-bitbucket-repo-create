Hubot Bitbucket Repo Creation
==========
A Hubot script to create bitbucket repos from Slack

Usage
-----

it simple, to create a repository just use the following command:
```
hubot create repo <name>
```

Configuration Settings or Environment Variables
-----------------------------------------------

this script can be configured with an [rc](https://github.com/dominictarr/rc/) config, or environment variables

#### rc config

copy the below config into the `hubot` directory, and name it `.slack_bitbucket_repo_createrc`, 

```
{
  "bearer_token_file": "/home/hubot/hubot/oauth/bitbucket.json",
  "organization": "sadasystems",
  "channel": "bitbucket-create-repo",
  "group": "acs"
}
```

#### env vars

`HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_BEARER_TOKEN_FILE` - raw json output from bitbucket api

`HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_ORGANIZATION` - which user or organization are we creating the repo for

`HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_GROUP` - scope the permissions to a certain user group (hard coded access is `write`)

`HUBOT_SLACK_BITBUCKET_REPO_CREATE_CHANNEL` - which channel will hubot respond to requests

Note
----
This is a public bitbucket repo so we can include it as a module.
Please do not commit any sensitive information to this repo.