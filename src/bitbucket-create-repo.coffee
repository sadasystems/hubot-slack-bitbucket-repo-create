# Description:
#   Create a bitbucket repository from a channel in Slack
#
# Dependencies
#   "request": "2.69.0"
#   "rc": "1.1.6 "
#
# Configuration:
#   HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_BEARER_TOKEN_FILE
#   HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_ORGANIZATION
#   HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_GROUP
#   HUBOT_SLACK_BITBUCKET_REPO_CREATE_CHANNEL
#
# Commands:
#   hubot create repo <name>
#
# Author:
#   John Lancaster

request = require 'request'

module.exports = (robot) ->

  config = require('rc')('slack_bitbucket_repo_create')

  token_file = process.env.HUBOT_SLACK_BITBUCKET_REPO_CREATE_BITBUCKET_BEARER_TOKEN_FILE || config?.bearer_token_file || '../../../bitbucket'
  organization = process.env.HUBOT_SLACK_BITBUCKET_ORGANIZATION || config?.organization || 'sadasystems'
  group = process.env.HUBOT_SLACK_BITBUCKET_GROUP || config?.group || 'acs'
  channel = process.env.HUBOT_SLACK_BITBUCKET_REPO_CREATE_CHANNEL || config?.channel || 'bot-test'

  # helper method to get sender of the message
  get_username = (response) ->
    "@#{response.message.user.name}"

  # helper method to get channel of originating message
  get_channel = (response) ->
    if response.message.room == response.message.user.name
      "@#{response.message.room}"
    else
      "#{response.message.room}"

  # A script to create bitbucket repos
  robot.respond /create repo/i, (msg) ->

    args = msg.message.text.split(' ')

    # make sure we get a repo name
    if (!args[3])
      robot.messageRoom channel, 'usage: `hubot create repo repo_name`'
      return

    # make sure our script only executes from the desired channel
    if(get_channel(msg) != channel)
      return

    new_repo = args[3]
    token = (require token_file).access_token
    user = get_username(msg).slice(1)

    create_opts =
        url: 'https://api.bitbucket.org/2.0/repositories/' + organization + '/' + new_repo
        headers:
          'Authorization': 'Bearer ' + token
        form:
          'scm': 'git'
          'is_private': 'true'
          'fork_policy': 'no_public_forks'

    access_opts =
        url: 'https://bitbucket.org/!api/1.0/group-privileges/' + organization + '/' + new_repo + '/' + organization + '/' + group + '/'
        headers:
          'Authorization': 'Bearer ' + token
        body: 'write'

    update_access = () ->
      request.put(access_opts, (err, res, body) =>
        if (err)
          robot.messageRoom channel, 'there was an error with your request `' + err + '`'

        if (res.statusCode != 200)
          robot.messageRoom channel, '`' + JSON.parse(body).error.message + '`'

        if (res.statusCode == 200)
          robot.messageRoom channel, 'created `' + new_repo + '` https://bitbucket.org/' + organization + '/' + new_repo + ' for group ' + group
      )

    request.post(create_opts, (err, res, body) =>
      if (err)
        robot.messageRoom channel, 'there was an error with your request `' + err + '`'

      if (res.statusCode != 200)
        robot.messageRoom channel, '`' + JSON.parse(body).error.message + '`'

      if (res.statusCode == 200)
        update_access()
    )
