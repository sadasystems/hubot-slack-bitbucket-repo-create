#/bin/bash

# 1. copy this file to hubot/cron/auth_bitbucket_tokens_cron.sh
#   `cp bitbucket_tokens_cron.sh /home/hubot/hubot/cron/auth_bitbucket_tokens.sh`
# 2. add the key and secret from bitbucket (need to be admin)
# 3. add `cron/auth_.*.sh` to `.gitignore`
#   so we don't capture any tokens in version control

# to obtain new tokens, cron this to run every hour on the 58th
# restart hubot after tokens are obtained on the hour
#
# 58 * * * * $HOME/auth_bitbucket_tokens.sh

curl -s -X POST -u "KEY:SECRET" \
              https://bitbucket.org/site/oauth2/access_token \
              -d grant_type=client_credentials > /home/hubot/hubot/oauth/bitbucket.json